const path = require('path');
const	HtmlWebpackPlugin = require('html-webpack-plugin');
const TSLintPlugin = require('tslint-webpack-plugin');
const ErrorOverlayPlugin = require('error-overlay-webpack-plugin');

module.exports = {
	entry: './src/index.tsx',
	output: {
		path: path.join(__dirname, '/dist'),
		filename: 'index_bundle.js'
	},
	devServer: {
		hot: true,
		port: 8081
	},
	resolve: {
		extensions: ['.js', '.jsx', '.json', '.ts', '.tsx']
	},
	module: {
		rules: [
			{
				test: /\.(ts|tsx)$/,
				loader: 'ts-loader',
			},
			{
				test: /\.styl$/,
				use: [
					{
						loader: 'style-loader', // creates style nodes from JS strings
					},
					{
						loader: 'css-loader', // translates CSS into CommonJS
					},
					{
						loader: 'stylus-loader', // compiles Stylus to CSS
					},
				],
			}
		]
	},
	devtool: 'cheap-module-source-map',
	plugins: [
		new HtmlWebpackPlugin({
			template : './src/index.html'
		}),
		new TSLintPlugin({
			files: ['./src/**/*.ts', './src/**/*.tsx'],
			silent: false,
			warningsAsError: true
		}),
		new ErrorOverlayPlugin()
	],
}