import * as Yup from "yup";

Yup.setLocale({
	mixed: {
		required: "Phai nhap gia tri"
	}
});

// tslint:disable-next-line: no-any
Yup.addMethod(Yup.string, "lasNameIsNguyen", function(this: any, arg) {
	const { message } = arg;
	// tslint:disable-next-line: no-any
	return this.test(message, function(this: any, value) {
		return value === "Nguyen" || this.createError({ path: this.path, message: message});
	});
});

export default Yup;
