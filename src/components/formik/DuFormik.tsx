import * as React from "react";
import { Formik, Field, Form } from "formik";
import Yup from "./DuValidation";

const initialValues = {
	firstName: "",
	lastName: "",
	email: "",
	password: "",
	confirmPassword: ""
};

const onSubmit = (values, {resetForm}) => {
	alert(JSON.stringify(values, null, 2));
	resetForm({});
};

const validationSchema = Yup.object({
	firstName: Yup.string()
		.max(15, "Must be 15 characters or less")
		.required()
		.test("firstNameIsDung", obj => `${obj.path} is not Dung` , value => {
			return value === "Dung";
		}),
	lastName: Yup.string()
		.max(20, "Must be 20 characters or less")
		.required()
		.lasNameIsNguyen({message: obj => `${obj.path} is not Nguyen`}),
	email: Yup.string().email("Invalid email address").required(),
	password: Yup.string()
		.required(),
	confirmPassword: Yup.string()
		.required()
		.oneOf([Yup.ref("password")], "Passwords must match")
});

const clonedSchema = validationSchema.clone().shape({
	firstName : Yup.string()
		.required(obj => `${obj.path} can phai nhap`)
		.matches(/(hi|bye)/, obj => `${obj.path} khong phu hop`)
		.label("ten")
		.meta({metaDataForFirstName: "Value of metadata for firstName"})
		.length(3)
});

const DuFormik = () => {
	const schema = Yup.object().shape({
		nested: Yup.object().shape({
			arr: Yup.array().of(Yup.object().shape(
				{
					num: Yup.number().max(4)
				}))
		})
	});
	schema.validate({nested: {arr: [{num: 1}, {num: 2}, {num: 3}]}}).catch(err => {});
	Yup.reach(schema, "nested[\"arr\"][1].num").validate(2).catch(err => {});

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={clonedSchema}
			onSubmit={onSubmit}
		>
			{formik => (
				<Form>
					<label htmlFor="firstName">First Name</label>
					<Field name="firstName" type="text"/>
					<label htmlFor="lastName">Last Name</label>
					<Field name="lastName" type="text"/>
					<label htmlFor="email">Email Address</label>
					<Field name="email" type="email"/>
					<label htmlFor="password">Pass word</label>
					<Field name="password" type="text"/>
					<label htmlFor="confirmPassword">Confirm password</label>
					<Field name="confirmPassword" type="text"/>
					<button type="submit">Submit</button>

					<br/>
					<pre>
					{JSON.stringify(formik, null, 4)}
				</pre>
				</Form>
			)

			}
		</Formik>);
};

export default DuFormik;
