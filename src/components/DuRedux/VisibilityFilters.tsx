import * as React from "react";
import { connect } from "react-redux";
import { VISIBILITY_FILTERS } from "./constants";
import { setFilter } from "../../redux/actions";

const VisibilityFilters = ({ activeFilter, setFilterProp }) => {
	return (
		<div className="visibility-filters">
			{Object.keys(VISIBILITY_FILTERS).map(filterKey => {
				const currentFilter = VISIBILITY_FILTERS[filterKey];
				return (
					<span
						key={`visibility-filter-${currentFilter}`}
						onClick={() => {setFilterProp(currentFilter); }}
					>
						{" " + currentFilter}
					</span>
				);
			})}
		</div>
	);
};

// export default VisibilityFilters;
const mapStateToProps = state => {
	return { activeFilter: state.visibilityFilter };
};
export default connect(
	mapStateToProps,
	{ setFilterProp: setFilter }
)(VisibilityFilters);
