import * as React from "react";
import AddTodo from "./AddTodo";
import TodoList from "./TodoList";
import VisibilityFilters from "./VisibilityFilters";

const DuRedux = () => {
	return (
		<div>
			<h1>Todo List</h1>
			<AddTodo />
			<TodoList todos={[]} />
			<VisibilityFilters/>
		</div>
	);
};

export default DuRedux;
