import * as React from "react";
import { connect } from "react-redux";
import { toggleTodo } from "../../redux/actions";

const Todo = ({ todo, toggleTodoProp }) => (
	<li
		className="todo-item"
		onClick={() => {
			toggleTodoProp(todo.id);
			}
		}
	>
		{todo && todo.completed ? "👌" : "👋"}{" "}
		<span>{todo.content}</span>
	</li>
);

export default connect(
	null,
	{ toggleTodoProp: toggleTodo }
)(Todo);
