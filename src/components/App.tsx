import * as React from "react";
import {
	HashRouter,
	Switch,
	Route,
	Link
} from "react-router-dom";
import DuFormik from "./formik/DuFormik";
import DuRedux from "./DuRedux/DuRedux";

class App extends React.Component {
	public render() {
		return (
			<HashRouter>
				<div id="app_wrapper">
					<div className="sidebar">
						<ul>
							<li><Link to={"/formik"}>Formik</Link></li>
							<li><Link to={"/redux"}>Redux</Link></li>
						</ul>
					</div>
					<div className="main_content">
						<div className="header">Learn react typescript</div>
						<div className="info">
							<Switch>
								<Route exact={true} path="/formik" component={DuFormik} />
								<Route exact={true} path="/redux" component={DuRedux} />
							</Switch>
						</div>
					</div>
				</div>
			</HashRouter>
		);
	}
}

export default App;
